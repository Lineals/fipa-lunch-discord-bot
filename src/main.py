import os
from command import Command
from dotenv import load_dotenv
from discord.ext import commands
from discord import Embed

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
COMMANDS = dict()
bot = commands.Bot(command_prefix='!')


@bot.command()
async def help_cmd(ctx):
    await ctx.message.channel.send(embed=Embed.from_dict(dict(
        title="Help",
        fields=[
            dict(name="!new {RestaurantName} {TimeLimit (h:m format)}", value="Crée une nouvelle commande"),
            dict(name="!add {RestaurantName} {Food Wanted}", value="Ajoute un élément à une commande")
        ]
    )))


@bot.command()
async def new(ctx):
    message = ctx.message
    print(message)
    message_text = message.content.split()
    if len(message_text) == 3:
        restaurant_name = message_text[1]
        time_limit = message_text[2]
        COMMANDS[restaurant_name] = Command(restaurant_name, time_limit)
        COMMANDS[restaurant_name].last_msg = await message.channel.send(embed=Embed.from_dict(COMMANDS[restaurant_name].to_dict()))
    await message.delete()


@bot.command()
async def close(ctx):
    message = ctx.message
    message_text = message.content.split()
    if len(message_text) == 2:
        restaurant_name = message_text[1]
        COMMANDS[restaurant_name].close()
        await COMMANDS[restaurant_name].last_msg.delete()
        COMMANDS[restaurant_name].last_msg = await message.channel.send(embed=Embed.from_dict(COMMANDS[restaurant_name].to_dict()))

    await message.delete()


@bot.command()
async def add(ctx):
    message = ctx.message
    message_text = message.content.split()
    if len(message_text) > 2:
        restaurant_name = message_text[1]
        food_wanted = " ".join(message_text[2:]).lower()
        COMMANDS[restaurant_name].add_new(food_wanted, message.author)
        await COMMANDS[restaurant_name].last_msg.delete()
        COMMANDS[restaurant_name].last_msg = await message.channel.send(embed=Embed.from_dict(COMMANDS[restaurant_name].to_dict()))

    await message.delete()


@bot.command()
async def clear(ctx):
    if str(ctx.message.author) == "Lineal#7791":
        async for message in ctx.message.channel.history(limit=200):
            if message.content.startswith("!") or message.author.bot:
                await message.delete()


if __name__ == "__main__":
    bot.run(TOKEN)
