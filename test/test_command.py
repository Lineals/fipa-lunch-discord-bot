from unittest import TestCase
from command import Command
from freezegun import freeze_time as ft


class TestCommand(TestCase):

    @ft("2020-01-14 12:00:00")
    def setUp(self):
        self.command = Command("NoodleShop", "12h15")

    @ft("2020-01-14 12:00:00")
    def test_add_new(self):
        self.command.add_new("Bo-Bun", "Lineal")
        self.assertEqual(self.command.component, {"Bo-Bun": ["Lineal"]})

    @ft("2020-01-14 12:20:00")
    def test_add_new_past(self):
        self.command.add_new("Bo-Bun", "Lineal")
        self.assertEqual(self.command.component, {})

    def test_dict_format(self):
        di = self.command.to_dict()
        self.assertIn("title", di.keys(), "Mandatory key for discord Embedded")
        self.assertIn("fields", di.keys(), "Mandatory key for discord Embedded")
        self.assertEqual([], di.get("fields"), "Fields should be empty")
        self.command.add_new("Bo-Bun", "Lineal")
        di = self.command.to_dict()
        self.assertEqual([{'inline': True, 'name': 'Bo-Bun', 'value': 'Lineal'}],
                         di.get("fields"),
                         "Fields should have one member")

    def test_close(self):
        self.command.close()
        self.assertTrue(self.command.closed)
        title = self.command.to_dict().get("title")
        self.assertIn("Commande fermée", title)
        self.assertNotIn("Commande ouverte", title)

