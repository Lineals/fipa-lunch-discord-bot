from datetime import datetime as dt


class Command:

    def __init__(self, name, time_limit):
        self.name = name
        self.time_stamp = dt.now().time()
        self.time_limit = dt.strptime(time_limit, '%Hh%M').time()
        self.last_msg = None
        self.closed = False
        self.component = {}

    def close(self):
        self.closed = True
        print(f"Comamnd {self.name} is now closed")

    def add_new(self, compo_name, person):
        if dt.now().time() < self.time_limit and not self.closed:
            if compo_name in self.component:
                self.component[compo_name].append(str(person))
            else:
                self.component[compo_name] = [str(person)]

    def to_dict(self):
        return dict(
            title=f"{self.name} - {self.time_limit} - Commande {'ouverte' if not self.closed else 'fermée'}",
            fields=list(dict(name=k, value="\n".join(v), inline=True) for k, v in self.component.items())
        )
